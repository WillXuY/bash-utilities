#!/bin/bash

# '$clean_token*' will be removed finally, don't name file matches that.
readonly clean_token="clean.temporary"
readonly text_year='2021-2022'
readonly text_readme_name='readme.md'
readonly text_program_name='learn-basic-c'
readonly dir_find_program='../learn-basic-c'
readonly match_file='.*\.py\|.*\.yml'
readonly match_line='Copyright (C)'
readonly operation_sed_insert=''

readonly text_sed_add="1i\\# Copyright (C) $text_year  Weiyang(Will) Xu\n\
# \n\
# This file is part of $text_program_name.\n\
# ${text_program_name^} is free software: you can redistribute it and/or modify\n\
# it under the term of the GNU General Public License version 3 or any\n\
# later version, as specified in the $text_readme_name file.\n
"

source function-add-license.bash
