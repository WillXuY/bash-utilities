#!/bin/bash

file_find_result="$clean_token.license.find"
find ./$dir_find_program -regex $match_file > $file_find_result

while read line_file_name
do
  file_match_line="$clean_token.match.sed"
  sed -n "/$match_line/ p" $line_file_name > $file_match_line
  if [ ! -s $file_match_line ]
  then
    sed $operation_sed_insert "$text_sed_add" $line_file_name
  fi
done < $file_find_result

rm $clean_token*
