# I - ⚖️ License

Copyright (C) 2021-2022  Weiyang(Will) Xu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

# II - 📖 What's this?
It is a bash utils collection.

# III -  📝 Content

|category|name| description|
|---|---|---|
|add-license|[c-like](add-license-c-like.bash)|insert line 1: /* Copyright (C) */|
| |[python-like](add-license-python-like.bash)|insert line 1: # Copyright (C)|
| |[markdown](add-license-markdown.bash)|insert line 1: # I - ⚖️ License|
| |[shell](add-license-shell-below-head.bash)|insert line 2: # Copyright (C)|
